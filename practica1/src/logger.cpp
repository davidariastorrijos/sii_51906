#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 60

int main()
{
	int fd;
	//char *fifo="/tmp/fifo";
	char buffer[MAX];
	unlink("/tmp/fifo");
	if(mkfifo("/tmp/fifo",0777)!=0){
		perror("mkfifo");
		exit(1);
	}

	fd=open("/tmp/fifo",O_RDONLY);
	if(fd==-1){
		perror("open");
		exit(1);
	}
	printf("Inicio del juego\n");
	while(1){
		if(read(fd,buffer,MAX)==-1){
			perror("read");
			exit(1);
		}
		else if(buffer[0]=='F'){
			printf("%s",buffer);
			break;
		}
		else{
			printf("%s \n", buffer);
		}
	}
	close(fd);
	unlink("/tmp/fifo");
	return 0;
}

